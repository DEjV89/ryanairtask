package pl.dejv.ryanairtask.repository

import pl.dejv.ryanairtask.api.RyanairApi
import pl.dejv.ryanairtask.model.StationsResponse
import pl.dejv.ryanairtask.socket.ApiSocket
import retrofit2.Callback

class StationsRepository {

		companion object {
				private val api: ApiSocket = ApiSocket.INSTANCE
				private const val STATIONS_URL = " https://tripstest.ryanair.com/static/stations.json"
		}

		fun getStations(callback: Callback<StationsResponse>) {
				api.create(RyanairApi::class.java)
								.getStations(STATIONS_URL)
								.enqueue(callback)
		}
}