package pl.dejv.ryanairtask.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.flight_item.view.*
import pl.dejv.ryanairtask.R
import pl.dejv.ryanairtask.model.FlightDetailsItem

interface FlightResultOnClickListener<T> {
		fun onItemClick(item: T)
}

class FlightsResultsAdapter(private val context: Context,
														private var data: FlightResults,
														private var priceFilterTop: Int,
														private val listener: FlightResultOnClickListener<FlightDetailsItem>) : RecyclerView.Adapter<FlightViewHolder>() {

		private var flights = data.flights
		private var flightDetails: ArrayList<FlightDetailsItem> = arrayListOf<FlightDetailsItem>()
		private var filteredList: ArrayList<FlightDetailsItem> = arrayListOf<FlightDetailsItem>()

		init {
				for (flight in flights) {
						val item = FlightDetailsItem(data.origin, data.destination, data.currency, flight, flight.regularFare?.fares?.get(0)?.amount)
						flightDetails.add(item)
				}
				filterOutByPrice()
		}

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightViewHolder {
				val view = LayoutInflater.from(context).inflate(R.layout.flight_item, parent, false)
				return FlightViewHolder(view)
		}

		override fun getItemCount(): Int = filteredList.size

		override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
				val item = getItem(position)
				holder.bind(item, listener)
		}

		private fun getItem(position: Int) = filteredList[position]

		fun setData(data: FlightResults) {
				this.data = data
				flights = data.flights

				for (flight in flights) {
						val item = FlightDetailsItem(data.origin, data.destination, data.currency, flight, flight.regularFare?.fares?.get(0)?.amount)
						flightDetails.add(item)
				}
				filterOutByPrice()
				notifyDataSetChanged()
		}

		fun updateFilter(value: Int) {
				priceFilterTop = value
				filteredList.clear()
				filterOutByPrice()
				notifyDataSetChanged()
		}

		private fun filterOutByPrice() {
				flightDetails.flatMap { flightDetailsItem ->
						if (flightDetailsItem.amount != null && flightDetailsItem.amount <= priceFilterTop) {
								filteredList.add(flightDetailsItem)
						}
						filteredList
				}
		}

}

class FlightViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
		private var flightDate = view.flightDate
		private var flightNumber = view.flightNumber
		private var duration = view.duration
		private var regularFarePrice = view.regularFarePrice

		@SuppressLint("SetTextI18n")
		fun bind(item: FlightDetailsItem, listener: FlightResultOnClickListener<FlightDetailsItem>) {

				flightNumber.text = item.flight.flightNumber
				flightDate.text = item.flight.time[0]
				duration.text = item.flight.duration

				val regularFare = item.flight.regularFare
				if (regularFare != null) {
						regularFarePrice.text = regularFare.fares[0].amount.toString() + EMPTY_STRING + item.currency
				} else {
						regularFarePrice.text = EMPTY_VALUE
				}

				view.setOnClickListener {
						listener.onItemClick(item)
				}

		}
}
