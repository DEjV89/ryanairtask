package pl.dejv.ryanairtask.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.flight_details_fragment.*
import pl.dejv.ryanairtask.R
import pl.dejv.ryanairtask.model.FlightDetailsItem

const val EMPTY_VALUE = " - "
const val EMPTY_STRING: String = ""

class FlightDetailsFragment : Fragment() {

		private lateinit var item: FlightDetailsItem

		companion object {
				fun getInstance() = FlightDetailsFragment()
		}

		override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
				return inflater.inflate(R.layout.flight_details_fragment, container, false)
		}

		override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
				super.onViewCreated(view, savedInstanceState)
				setupView()
		}

		fun setData(item: FlightDetailsItem) {
				this.item = item
		}

		private fun setupView() {
				stationOrigin.text = item.origin
				stationDestination.text = item.destination
				infantsLeft.text = item.flight.infantsLeft.toString()

				val regularFare = item.flight.regularFare
				if (regularFare != null) {
						fareClass.text = regularFare.fareClass
						discountInPercent.text = regularFare.fares[0].discountInPercent.toString()
				} else {
						fareClass.text = EMPTY_VALUE
						discountInPercent.text = EMPTY_VALUE
				}
		}

}
