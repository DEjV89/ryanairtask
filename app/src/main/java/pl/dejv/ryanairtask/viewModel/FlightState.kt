package pl.dejv.ryanairtask.viewModel

data class FlightState private constructor(val status: FlightStatus,
																					 val error: Throwable? = null) {

		companion object {
				fun searching(): FlightState {
						return FlightState(FlightStatus.SEARCHING)
				}

				fun success(): FlightState {
						return FlightState(FlightStatus.SUCCESS)
				}

				fun unsuccessful(): FlightState {
						return FlightState(FlightStatus.UNSUCCESSFUL)
				}

				fun error(error: Throwable?): FlightState {
						return FlightState(FlightStatus.ERROR, error)
				}
		}
}

enum class FlightStatus {
		SEARCHING,
		SUCCESS,
		UNSUCCESSFUL,
		ERROR,

}
