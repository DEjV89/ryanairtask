package pl.dejv.ryanairtask.api

import pl.dejv.ryanairtask.model.FlightResultsResponse
import pl.dejv.ryanairtask.model.StationsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface RyanairApi {

		/**
		 * API to pass in : https://tripstest.ryanair.com/static/stations.json
		 */
		@GET()
		fun getStations(@Url url: String): Call<StationsResponse>

		@GET("api/v3/Availability")
		fun search(@Query("origin") origin: String,
							 @Query("destination") destination: String,
							 @Query("dateout") dateOut: String,
							 @Query("datein") dateIn: String = "",
							 @Query("flexdaysbeforeout") flexdaysbeforeout: Int = 3,
							 @Query("flexdaysout") flexdaysout: Int = 3,
							 @Query("flexdaysbeforein") flexdaysbeforein: Int = 3,
							 @Query("flexdaysin") flexdaysin: Int = 3,
							 @Query("adt") adult: Int = 1,
							 @Query("teen") teen: Int = 0,
							 @Query("chd") child: Int = 0,
							 @Query("roundtrip") roundtrip: Boolean = false,
							 @Query("ToUs") termsOfUse: String = "AGREED"
							 )
						: Call<FlightResultsResponse>
}