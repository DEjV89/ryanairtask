package pl.dejv.ryanairtask.socket

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.dejv.ryanairtask.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiSocket private constructor() {

		private val retrofit: Retrofit

		companion object {
				private const val SOCKET_TIMEOUT_SEC = 20L
				private const val RYANAIR_BASE_URL = BuildConfig.SERVER
				val INSTANCE: ApiSocket by lazy { ApiSocket() }

				private val headerInterceptor = Interceptor { chain ->
						val original = chain.request()

						val request = original.newBuilder()
										.header("Content-type", "application/json")
										.method(original.method(), original.body())
										.build()

						chain.proceed(request)
				}
		}

		init {
				val logging = HttpLoggingInterceptor()
				logging.level = HttpLoggingInterceptor.Level.BODY

				val httpClient = OkHttpClient.Builder()
								.addInterceptor(headerInterceptor)
								.addInterceptor(logging)
								.connectTimeout(SOCKET_TIMEOUT_SEC, TimeUnit.SECONDS)
								.build()

				retrofit = Retrofit.Builder()
								.baseUrl(RYANAIR_BASE_URL)
								.addConverterFactory(GsonConverterFactory.create())
								.client(httpClient)
								.build()
		}

		fun <API : Any> create(clazz: Class<API>): API {
				return retrofit.create(clazz)
		}

}


